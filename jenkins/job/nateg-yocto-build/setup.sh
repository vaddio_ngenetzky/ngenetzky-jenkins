
create_jenkins_id() {
    date --iso-8601="seconds" | sed 's/T/_/' | sed 's/:/-/g' | sed 's/-0600//'
}

################################################################################
# Jenkins
#
export JENKINS_HOME="/home/nateg/workspaces/jenkins/"

export WORKSPACE="$(pwd)"
export BUILD_NUMBER="1"
export BUILD_ID="$(create_jenkins_id)"
#
################################################################################

################################################################################
# Parameters
#
export VNG_REPO="git@bitbucket.org:vaddio/vaddio_ngenetzky-vng.git"
export COMMIT="master"
export MACHINE="cam01-sitara"
export BUILD_IMAGE="vaddio-core-debug-image"
export FILESYSTEM_IMAGE_VERSION="debug"
# export VAD_PRODUCT_VERSION=""
export MW_SERVICE_SELF_START=""
#
################################################################################

# export INCLUDE_PYTHON_PACKAGES_FLAG="1"
# export INCLUDE_BOOTLOADERS_FLAG="1"
# export INCLUDE_COMPLETE_UPDATER_FLAG="1"

unset create_jenkins_id


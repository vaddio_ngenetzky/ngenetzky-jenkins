#!/bin/bash

buildhistory_is_enabled(){
    [[ ! -z "${BUILDHISTORY_BRANCH?}" ]]
}

buildhistory_git_setup(){
    local branch="${1-master}"
    local bhdir="${WORKSPACE?}/build/buildhistory"

    git clone --depth 1 \
        --branch "${branch}" \
        "git@bitbucket.org:vaddio_ngenetzky/yocto-buildhistory.git" \
        "${bhdir}"
    git config --file "${bhdir}/.git/config" \
        user.email "vaddio.builder@milestone.com"
    git config --file "${bhdir}/.git/config" \
        user.name "Vaddio Builder"
}

buildhistory_add_to_local_sample(){
    local templateconf="${1?}"
    local sample_conf="${WORKSPACE?}/${templateconf}/local.conf.sample"
    [[ -f ${sample_conf} ]] || return 1
cat << EOF >> "${sample_conf}"
INHERIT += "buildhistory"
BUILDHISTORY_COMMIT = "1"
EOF
}

buildhistory_pre() {
    buildhistory_git_setup "${BUILDHISTORY_BRANCH?}"
    buildhistory_add_to_local_sample "meta-milestone/conf"
    buildhistory_add_to_local_sample "meta-vaddio-ark/conf"
    buildhistory_add_to_local_sample "meta-vaddio/conf"
}

if buildhistory_is_enabled ; then
    buildhistory_pre
fi



cp_deploydirimage_to_artifacts()
{
    local deploydir="${WORKSPACE?}/build/tmp/deploy/images/"
    local artifactsdir="${ARTIFACTS_DIR?}"
    cp -R -t "${artifactsdir}" \
        "${deploydir}"
}

ARTIFACTS_DIR=${WORKSPACE}/archive/${BUILD_ID}/artifacts
mkdir -p ${ARTIFACTS_DIR}

if [ "$INCLUDE_PYTHON_PACKAGES" = "true" ]; then
    INCLUDE_PYTHON_PACKAGES_FLAG="--include-python-packages"
fi

if [ "$INCLUDE_BOOTLOADERS" = "true" ]; then
    INCLUDE_BOOTLOADERS_FLAG="--include-bootloaders"
fi

if [ "$INCLUDE_BOOTLOADERS" = "true" ]; then
    INCLUDE_BOOTLOADERS_FLAG="--include-bootloaders"
fi

if [[ ${BUILD_IMAGE:+1} && ${FILESYSTEM_IMAGE_VERSION:+1} ]]; then
    # Build artifacts if the correct variables are set.
    ${WORKSPACE}/tools/build-scripts-ark/vng-create-build-artifacts \
        --workspace ${WORKSPACE}/ \
        --machine ${MACHINE} \
        --build-image ${BUILD_IMAGE} \
        --filesystem-image-version ${FILESYSTEM_IMAGE_VERSION} \
        --dest-dir ${ARTIFACTS_DIR} \
        ${INCLUDE_PYTHON_PACKAGES_FLAG} \
        ${INCLUDE_BOOTLOADERS_FLAG} \
        --include-firmware ${INCLUDE_COMPLETE_UPDATER_FLAG}
else
    cp_deploydirimage_to_artifacts
fi

if [ "$INCLUDE_API_DOC" = "true" ]; then
    if ls -U ${WORKSPACE}/build/tmp/deploy/images/${MACHINE}/*.pdf ; then
        cp ${WORKSPACE}/build/tmp/deploy/images/${MACHINE}/*.pdf ${ARTIFACTS_DIR}
    fi
fi

if [ "$INCLUDE_LICENSE_MANIFEST" = "true" ]; then
    if [ "$FILESYSTEM_IMAGE_VERSION" = "debug" ] || [ "$FILESYSTEM_IMAGE_VERSION" = "all" ]; then
        cp ${WORKSPACE}/build/tmp/deploy/licenses/${BUILD_IMAGE}-debug-image-${MACHINE}*/license.manifest ${ARTIFACTS_DIR}/${BUILD_IMAGE}-debug-image-${MACHINE}-license.manifest
    fi

    if [ "$FILESYSTEM_IMAGE_VERSION" = "production" ] || [ "$FILESYSTEM_IMAGE_VERSION" = "all" ]; then
        cp ${WORKSPACE}/build/tmp/deploy/licenses/${BUILD_IMAGE}-image-${MACHINE}*/license.manifest ${ARTIFACTS_DIR}/${BUILD_IMAGE}-image-${MACHINE}-license.manifest
    fi

    ls ${WORKSPACE}/build/tmp/deploy/licenses/
    ls ${WORKSPACE}/build/tmp/deploy/licenses/*
fi


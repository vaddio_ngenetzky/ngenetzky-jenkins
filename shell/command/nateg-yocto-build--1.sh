# Export the local ref, so the clone can use the local reference directory to reduce network usage
export GIT_LOCAL_REF_DIR="${JENKINS_HOME}/yocto"

export SSTATE_MOUNT="${JENKINS_HOME}/share/containerized/sstate:/var/build/build/sstate-cache" 

mkdir -p ${WORKSPACE}/build/sstate-cache

export VAD_SCM_REPO=$(echo "$VNG_REPO" | sed 's!:!/!')
export VAD_SCM_COMMIT="$COMMIT"

if [ -z "$VAD_PRODUCT_VERSION" ]; then
    VAD_PRODUCT_VERSION="jenkins-$BUILD_NUMBER"
fi

BUILD=${WORKSPACE}/tools/build-scripts-ark/vng-containerized-build

# vng is cloned into subdirectory.
ln -sT './vng/tools' './tools'
ln -sT './vng/meta-vaddio-ark' './meta-vaddio-ark'

# RECIPE is intentionally not quoted.
sh -x $BUILD \
    --machine "${MACHINE}" \
    --branch "${COMMIT}" \
    --filesystem-image ${RECIPE}

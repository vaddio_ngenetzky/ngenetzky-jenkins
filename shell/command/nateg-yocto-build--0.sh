set +ex

# Default state of things
echo "FAILED" > ${WORKSPACE}/build.status

BITBUCKET_REPO=`echo ${VNG_REPO} | sed 's/git@bitbucket.org:vaddio\///' | sed 's/.git//'`
STATE="INPROGRESS"
VERSION_INFO="MACHINE=${MACHINE}, RECIPE=${RECIPE}"

post_update_to_bitbucket() {
    local key="${MACHINE}-${RECIPE}"
    local desc="${VERSION_INFO}"
    curl -H "Content-Type: application/json" -X POST \
        -d "{\"state\":\"${STATE}\",\"key\":\"${key}\",\"name\":\"${BUILD_DISPLAY_NAME}\",\"url\":\"${BUILD_URL}\",\"description\":\"${desc}\"}" \
        --user vaddio_builder:Vaddio2009 \
        https://api.bitbucket.org/2.0/repositories/vaddio/${BITBUCKET_REPO}/commit/${COMMIT}/statuses/build \
        2> /dev/null
}

post_update_to_bitbucket

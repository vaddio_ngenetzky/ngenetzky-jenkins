#!/bin/bash
buildhistory_is_enabled(){
    [[ ! -z "${BUILDHISTORY_BRANCH?}" ]]
}

buildhistory_post(){
    local branch="${1-${JOB_NAME}/${BUILD_NUMBER}}"

    local bhdir="${WORKSPACE}/build/buildhistory"
    mkdir -p "${bhdir}"
    cd "${bhdir}"
    git checkout -b "$branch"
    git push origin "$branch:$branch"
}


if buildhistory_is_enabled ; then
    buildhistory_post
fi
